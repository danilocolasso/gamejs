Images = function() {
    var self = {
        player: new Image(),
        enemy: new Image(),
        bullet: new Image(),
        upgrade1: new Image(),
        upgrade2: new Image(),
    };

    //Autoload all images in config file
    for(key in self) {
        self[key].src = IMG_SRC[key];
    };

    return self;
};