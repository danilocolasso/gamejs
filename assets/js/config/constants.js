var GAME_CONTAINER  = document.getElementById('game');
var SCREEN          = { WIDTH: 500, HEIGHT: 500 };
var FPS             = 40;
var ANIMATION_RATE  = 0.08;
var TILE_SIZE       = 32;
var KEY_CODE        = {
    UP: 87,
    DOWN: 83,
    LEFT: 65,
    RIGHT: 68,
    PAUSE: 80
};
var IMG_SRC         = {
    player: "assets/img/players.png",
    enemy: "assets/img/enemies.png",
    bullet: "assets/img/bullet.png",
    upgrade1: "assets/img/upgrade1.gif",
    upgrade2: "assets/img/upgrade2.png",
}