Game = function() {
    var self = {
        canvas: null,
        interface: {},
        paused: false,
        frame: { count: 0 },
        score: 0
    };
    
    self.init = function() {
        //Create canvas element
        self.canvas         = document.createElement('canvas');
        self.canvas.width   = SCREEN.WIDTH;
        self.canvas.height  = SCREEN.HEIGHT;
        self.canvas.id      = 'ctx';
        self.canvas.style   = 'border: 1px solid #000000';
        self.ctx            = self.canvas.getContext('2d');;
        self.ctx.font       = '30px Arial';
        GAME_CONTAINER.appendChild(self.canvas);

        //Create Inventory Interface
        self.interface.inventory    = document.createElement('div');
        self.interface.inventory.id = 'inventory';
        GAME_CONTAINER.appendChild(self.interface.inventory);

        //Whatch inputs
        self.initInputs();

        //Draw the game
        setInterval(game.update, FPS);

        //Starts a new game
        self.new();
    };

    self.initInputs = function() {
        document.onmousedown = function(e) {
            if(e.which === 1)
                player.mouse.left = true;
            else
                player.mouse.right = true;
        }

        document.onmouseup = function(e) {
            player.mouse.left  = false;
            player.mouse.right = false;
        }

        document.onclick = function(e){
            
        }
        
        document.oncontextmenu = function(e){
            e.preventDefault();
        }

        document.onmousemove = function(e){
            var mouseX = e.clientX - self.canvas.getBoundingClientRect().left;
            var mouseY = e.clientY - self.canvas.getBoundingClientRect().top;
            
            mouseX -= SCREEN.WIDTH / 2;
            mouseY -= SCREEN.HEIGHT / 2;
            
            player.attack.angle = Math.atan2(mouseY, mouseX) / Math.PI * 180;
        }
        
        document.onkeydown = function(event){
            player.move(event.keyCode, true);
            if(event.keyCode === KEY_CODE.PAUSE) {
                self.paused = !self.paused;
            }
        }
        
        document.onkeyup = function(event){
            player.move(event.keyCode, false);
        }
    };

    self.update = function() {
        if(self.paused) {
            self.ctx.fillText('PAUSED', SCREEN.HEIGHT / 2, SCREEN.WIDTH / 2);
            return;
        }

        self.ctx.clearRect(0, 0, SCREEN.WIDTH, SCREEN.HEIGHT);
        Maps.current.draw();
        self.frame.count++;
        self.score++;
        
        if(self.frame.count % 100 === 0)      //every 4 sec
            Enemy.random();

        if(self.frame.count % 75 === 0)       //every 3 sec
            Upgrade.random();
        
        Bullet.update();
        Upgrade.update();
        Enemy.update();
        player.update();
        
        self.ctx.fillText(player.hp + " Hp",0,30);
        self.ctx.fillText('Score: ' + self.score,200,30);
    };

    self.new = function () {
        player.hp           = 10;
        self.frame.count    = 0;
        self.score          = 0;
        Enemy.List          = {};
        Upgrade.List        = {};
        Bullet.List         = {};

        Enemy.random();
        Enemy.random();
        Enemy.random();   
    };

    return self;
};