Entity = function(type, id, x, y, width, height, img) {
    var self = {
        type:type,
        id:id,
        x:x,
        y:y,
        width:width,
        height:height,
        img:img,
        playerOffset: {
            x: 0,
            y: 0
        },
        animation: {
            moving: 0
        }
    };

    self.update = function(){
        self.updatePosition();
        self.draw();
    }

    self.draw = function(){
        game.ctx.save();

        self.playerOffset.x = self.x - player.x;
        self.playerOffset.y = self.y - player.y;

        self.playerOffset.x += SCREEN.WIDTH / 2;
        self.playerOffset.y += SCREEN.HEIGHT / 2;

        self.playerOffset.x -= self.width / 2;
        self.playerOffset.y -= self.height / 2;

        game.ctx.drawImage(
            self.img,
            0, 0,
            self.img.width, self.img.height,
            self.playerOffset.x, self.playerOffset.y,
            self.width, self.height
        );

        game.ctx.restore();
    }

    self.getDistance = function(entity) {
        var vx = self.x - entity.x;
        var vy = self.y - entity.y;
        return Math.sqrt(vx*vx+vy*vy);
    }

    self.testCollision = function(entity) {
        var a = {
            x: self.x - self.width / 2,
            y: self.y - self.height / 2,
            width: self.width,
            height: self.height,
        }
        var b = {
            x: entity.x - entity.width / 2,
            y: entity.y - entity.height / 2,
            width: entity.width,
            height: entity.height,
        }

        return a.x <= b.x + b.width
            && b.x <= a.x + a.width
            && a.y <= b.y + b.height
            && b.y <= a.y + a.height
        ;
    }

    self.updatePosition = function() { }

    return self;
}