
Maps = function(id, imgSrc, grid) {
    var self = {
        id: id,
        image: new Image(),
        width: grid[0].length * TILE_SIZE, //Size of first index
        height: grid.length * TILE_SIZE, //Size matriz
        grid: grid
    };

    self.image.src = imgSrc;

    self.invalidPosition = function(pt) {
        var gridX = Math.floor(pt.x / TILE_SIZE);
		var gridY = Math.floor(pt.y / TILE_SIZE);

		if(gridX < 0 || gridX >= self.grid[0].length)
			return true;
		if(gridY < 0 || gridY >= self.grid.length)
			return true;
            
		return self.grid[gridY][gridX];
    };

    self.draw = function() {
        //If player x increase then map x decrease
        var x = SCREEN.WIDTH / 2 - player.x;
        var y = SCREEN.HEIGHT / 2 - player.y;

        //image, cropStartX, cropStartY, cropWidth, cropHeight, drawX, drawY, drawWidth, drawHeight
        game.ctx.drawImage(self.image, 0, 0, self.width, self.height, x, y, self.width, self.height);
        
    };

    return self;
};