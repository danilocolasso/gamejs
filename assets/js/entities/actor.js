 
Actor = function(type, id, x, y, width, height, sprite, hp, atkSpd) {
        var self = Entity(type, id, x, y, width, height, sprite.image);
       
        self.hp     = hp;
        self.hpMax  = hp;
        self.speed  = 3;
        self.attack = {
            angle: 0,
            speed: atkSpd,
            counter: 0
        };
        self.pressing = {
            down: false,
            up: false,
            left: false,
            right: false,
        };
        self.sprite = {
            image: sprite.image,
            frames: sprite.frame || [3, 4],  //3 animations and 4 posible positions: up, down, right and left
            position: sprite.position || {
                column: 1,
                row: 1
            },
            start: sprite.start || {
                column: 1,
                row: 1
            }    
        };
        self.halfWidht  = self.width / 2;
        self.halfHeight = self.height / 2;

        //Calc ALL sprite dimensions

        //It starts with 0
        self.sprite.position.column -= 1;
        self.sprite.position.row    -= 1;
        self.sprite.start.row       -= 1;
        self.sprite.start.column    -= 1;

        //Calc the dimensions of the block
        self.sprite.sheet = {
            x: self.sprite.frames[0] * self.width,
            y: self.sprite.frames[1] * self.height
        };
        
        //Define it start point
        self.sprite.crop = {
            x: self.sprite.sheet.x * self.sprite.position.column,
            y: self.sprite.sheet.y * self.sprite.position.row
        };
       
        var _update = self.update;
        self.update = function(){
            _update();

            if(self.hp <= 0) {
                self.die();
                return;
            }

            self.attack.counter += self.attack.speed;
        }
        
        self.calcPlayerOffset = function() {
            self.playerOffset.x = self.x - player.x;
            self.playerOffset.y = self.y - player.y;

            self.playerOffset.x += SCREEN.WIDTH / 2;
            self.playerOffset.y += SCREEN.HEIGHT / 2;

            self.playerOffset.x -= self.width / 2;
            self.playerOffset.y -= self.height / 2;
        };

        //Overwrite draw
        self.draw = function(){
            game.ctx.save();

            self.calcPlayerOffset();

            var angle = self.attack.angle;
            if(angle < 0) angle += 360; //Always positive

            var directionMod = 2; //Default Right
            if(angle >= 45 && angle < 135) directionMod = 0;  //Down
            if(angle >= 135 && angle < 225) directionMod = 1; //Left
            if(angle >= 225 && angle < 315) directionMod = 3; //Top
            
            //Walking animate
            var walkingMod = Math.floor(self.animation.moving) % self.sprite.frames[0];

            var frameWidth  = self.sprite.crop.x + (walkingMod * self.width);
            var frameHeight = self.sprite.crop.y + (directionMod * self.height);


            game.ctx.drawImage(
                self.img,
                frameWidth, frameHeight,
                self.width, self.height,
                self.playerOffset.x, self.playerOffset.y,
                self.width, self.height
            );

            game.ctx.restore();
        }

        self.updatePosition = function() {
            self.bumper = {
                left:   { x: self.x - self.halfWidht, y: self.y },
                right:  { x: self.x + self.halfWidht, y: self.y },
                up:     { x: self.x, y: self.y - self.halfHeight },
                down:   { x: self.x, y: self.y + self.halfHeight }
            };

            //Move
            if(Maps.current.invalidPosition(self.bumper.right)) {
                self.x -= self.speed;
            } else {
                if(self.pressing.right)
                    self.x += self.speed;
            }
            
            if(Maps.current.invalidPosition(self.bumper.left)) {
                self.x += self.speed; 
            } else {
                if(self.pressing.left)
                    self.x -= self.speed; 
            }
             
            if(Maps.current.invalidPosition(self.bumper.down)) {
                self.y -= self.speed;
            } else {
                if(self.pressing.down)
                    self.y += self.speed;
            }
            
            if(Maps.current.invalidPosition(self.bumper.up)) {
                self.y += self.speed;
            } else {
                if(self.pressing.up)
                    self.y -= self.speed;
            }
            

            //Animate walking
            if(self.pressing.right || self.pressing.left || self.pressing.up || self.pressing.down)
                self.animation.moving += ANIMATION_RATE * self.speed;
            
            //check id position is valid
            if(self.x < self.width / 2)                         self.x = self.width / 2;
            if(self.x > Maps.current.width - self.width / 2)    self.x = Maps.current.width - self.width / 2;
            if(self.y < self.height / 2)                        self.y = self.height / 2;
            if(self.y > Maps.current.height - self.height / 2)  self.y = Maps.current.height - self.height / 2;
        }

        self.die = function() {};
       
        self.performAttack = function() {
            if(self.attack.counter > 25) {
                self.attack.counter = 0;
                Bullet.generate(self);
            }
        }
       
        self.performSpecialAttack = function() {
            if(self.attack.counter > 50) {
                self.attack.counter = 0;
                /*
                for(var i = 0 ; i < 360; i++){
                        Bullet.generate(self,i);
                }
                */
                Bullet.generate(self, self.attack.angle - 5);
                Bullet.generate(self, self.attack.angle);
                Bullet.generate(self, self.attack.angle + 5);
            }
        }
 
       
        return self;
}