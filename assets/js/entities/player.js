Player = function() {
    var sprite = {
        image: Img.player,
        position: { column: 3, row: 1 },
        start: { column: 2, row: 1 }    
    };

    var self = Actor('player', 'myId', 50, 40, 32, 32, sprite, 10, 1);
    
    self.inventory = Inventory();
    
    self.speed  = 10;
    self.mouse = {
        left: false,
        right: false
    };

    var _update = self.update;
    self.update = function() {
        _update();

        if(self.mouse.left)
            self.performAttack();
        if(self.mouse.right)
            self.performSpecialAttack();
    };

    self.die = function() {
        if(self.hp <= 0){
            console.log("Game Over.");            
            game.new();
        }
    };
    
    self.move = function(key, pressing) {
        switch(key) {
            case KEY_CODE.UP:
                self.pressing.up = pressing;
            break;
            
            case KEY_CODE.RIGHT:
                self.pressing.right = pressing;
            break;
            
            case KEY_CODE.DOWN:
                self.pressing.down = pressing;
            break;

            case KEY_CODE.LEFT:
                self.pressing.left = pressing;
            break;
        }
    };

    return self;     
};