Upgrade = function (id, x, y, width, height, category, img){
    var self = Entity('upgrade', id, x, y, width, height, img);

    var _update = self.update;
    self.update = function() {
        _update();
        var isColliding = player.testCollision(self);
        if(isColliding){
            if(self.category === 'score')
                game.score += 1000;
            if(self.category === 'atkSpd')
                player.attack.speed += 3;
            delete Upgrade.List[self.id];
        }
    };
    
    self.category   = category;
    Upgrade.List[id] = self;
}
Upgrade.List = {};
Upgrade.update = function() {
    for(var key in Upgrade.List){
        Upgrade.List[key].update();
    }
};
Upgrade.random = function() {
    //Math.random() returns a number between 0 and 1
    var x       = Math.random() * Maps.current.width;
    var y       = Math.random() * Maps.current.height;
    var height  = 30;
    var width   = 30;
    var id      = Math.random();
    
    if(Math.random() < 0.5){
        var category = 'score';
        var img = Img.upgrade2;
    } else {
        var category = 'atkSpd';
        var img = Img.upgrade1;
    }
    
    Upgrade(id, x, y, width, height, category, img);
};