Bullet = function (id, x, y, spdX, spdY, width, height, combatType) {
    var self        = Entity('bullet', id, x, y, width, height, Img.bullet);
    self.timer      = 0;
    self.rate       = 30;
    self.range      = 50;
    self.speed      = 20;
    self.upgrade    = 2;
    self.combatType = combatType;
    self.spdX       = spdX;
    self.spdY       = spdY;

    self.updatePosition = function() {
        self.x += self.spdX;
        self.y += self.spdY;

        //Delete bullet when hit the end of the map
        if(
            (self.x < (self.width / 2) || self.x > Maps.current.width - (self.width / 2))
            ||
            (self.y < (self.height / 2) || self.y > Maps.current.height - (self.height / 2))
         ) {
            delete Bullet.List[self.id];
        }
    }

    var _update = self.update;
    self.update = function() {
        _update();

        self.toRemove = false;
        self.timer++;

        if(
            self.x < 0
            || self.x > Maps.current.width
            || self.y < 0
            || self.y > Maps.current.height
            || self.timer > self.range
            || Maps.current.invalidPosition(self)
        ) {
            self.toRemove = true;
        }

        if(self.combatType === 'player') {
            for(var key2 in Enemy.List) {
                var isColliding = self.testCollision(Enemy.List[key2]);
                if(isColliding) {
                    self.toRemove = true;
                    Enemy.List[key2].hp -= 1;
                    break;
                }      
            }
        } else if(self.combatType === 'enemy') {
            for(var key2 in Enemy.List) {
                var isColliding = self.testCollision(player);
                if(isColliding) {
                    self.toRemove = true;
                    player.hp -= 1;
                    break;
                }      
            }
        }
    };

    Bullet.List[id] = self;
}
Bullet.List = {};
Bullet.update = function() {
    for(var key in Bullet.List) {
        var bullet = Bullet.List[key];
        bullet.update();
        
        if(bullet.toRemove){
            delete Bullet.List[key];
        }
    }
};
Bullet.generate = function(actor, aimOverwrite) {
    //Math.random() returns a number between 0 and 1
    var x       = actor.x;
    var y       = actor.y;
    var height  = 15;
    var width   = 15;
    var id      = Math.random();
    
    var angle = actor.attack.angle;
    if(aimOverwrite !== undefined)
        angle = aimOverwrite;
    
    var spdX = Math.cos(angle/180*Math.PI)*5;
    var spdY = Math.sin(angle/180*Math.PI)*5;

    Bullet(id, x, y, spdX, spdY, width, height, actor.type);
};