 
Enemy = function(id, x, y, width, height, sprite, hp, atkSpd) {
    var self = Actor('enemy', id, x, y, width, height, sprite, hp, atkSpd);
    Enemy.List[id] = self;

    self.die = function() {
        delete Enemy.List[self.id];
        return;
    };

    //Simple AI for aim angle
    self.updateAim = function() {
        var diffX = player.x - self.x;
        var diffY = player.y - self.y;

        self.attack.angle = Math.atan2(diffY, diffX) / Math.PI * 180;
    };
    //Simple AI for movement
    self.updateKeyPress = function() {
        var diffX           = player.x - self.x;
        var diffY           = player.y - self.y;
        var playerHalfSpeed = player.speed / 2;

        self.pressing.right = diffX > playerHalfSpeed;
        self.pressing.left  = diffX < -playerHalfSpeed;
        self.pressing.down  = diffY > playerHalfSpeed;
        self.pressing.up    = diffY < -playerHalfSpeed;
    };
    
    self.drawHp = function() {
        game.ctx.save();

        game.ctx.fillStyle   = 'red';
        game.ctx.strokeStyle = 'black';

        var total   = (self.width * 2);
        var partial = total * self.hp / self.hpMax;
        var height  = 10;
        var x       = self.playerOffset.x - self.width / 2;
        var y       = self.playerOffset.y - 5;

        if(width < 0) width = 0;

        game.ctx.fillRect(x, y, partial, height);
        game.ctx.strokeRect(x, y, total, height);

        game.ctx.restore();
    };

    var _draw = self.draw;
    self.draw = function() {
        _draw();
        self.drawHp();
    };

    var _update = self.update;
    self.update = function() {
        _update();

        self.animation.moving += ANIMATION_RATE * self.speed;
        self.updateAim();
        self.updateKeyPress();
        self.performAttack();
    };
}
Enemy.List = {};
Enemy.update = function() {
    for(var key in Enemy.List){
        Enemy.List[key].update();
    }
};
Enemy.random = function() {
    //Math.random() returns a number between 0 and 1
    var x       = Math.random() * Maps.current.width;
    var y       = Math.random() * Maps.current.height;
    var height  = 32;
    var width   = 32;
    var id      = Math.random();
    var hp      = 2;
    var atkSpd  = 1;
    var sprite  = {
        image : Img.enemy,
        position: { column: 1, row: 1 },
        start: { column: 2, row: 1 }    
    };

    if(Math.random() < 0.5) {
        sprite.position = { column: 4, row: 1 };
        hp              = 1;
        atkSpd          = 3;
    }

    Enemy(id, x, y, width, height, sprite, hp, atkSpd);
};